package com.cdiaz.cabifychallenge.skeleton.presenters.contracts;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by carlos on 20/03/2018.
 */

public interface CabifymapFragmentPresenterContract {

    void getEstimate(LatLng start, LatLng stop);

}
