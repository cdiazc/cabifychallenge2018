package com.cdiaz.cabifychallenge.skeleton.views.fragments.contracts;

import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;

import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public interface GenericContract {

    void onInformationReceived(List<CabifyResponse> responseList);
    void onError(String message);
}
