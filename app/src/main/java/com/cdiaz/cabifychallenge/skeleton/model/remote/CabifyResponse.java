package com.cdiaz.cabifychallenge.skeleton.model.remote;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by carlos on 20/03/2018.
 */

public class CabifyResponse {

    @SerializedName("message")
    public String message;

    @SerializedName("price_formatted")
    public String priceString;

    @SerializedName("vehicle_type")
    public VehicleType vehicleType;

}
