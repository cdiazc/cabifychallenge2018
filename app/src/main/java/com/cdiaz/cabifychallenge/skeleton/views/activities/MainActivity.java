package com.cdiaz.cabifychallenge.skeleton.views.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.cdiaz.cabifychallenge.R;
import com.cdiaz.cabifychallenge.databinding.ActivityMainBinding;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.CabifymapFragment;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.InfoFragment;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.ResultsFragment;
import com.cdiaz.cabifychallenge.util.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private CabifymapFragment cabifymapFragment;
    private InfoFragment infoFragment;
    private ResultsFragment resultsFragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        cabifymapFragment = new CabifymapFragment();
        infoFragment = new InfoFragment();
        resultsFragment = new ResultsFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        binding = null;
        cabifymapFragment = null;
        infoFragment = null;

    }

    @Override
    protected void onResume() {
        super.onResume();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_map_container, cabifymapFragment)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_info_container, infoFragment)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_results_container, resultsFragment)
                .commit();

    }



    public void displayLoadingScreen() {
        binding.activityMainInfoLoading.setVisibility(View.VISIBLE);
    }

    public void hideLoadingScreen() {
        binding.activityMainInfoLoading.setVisibility(View.GONE);
    }

    public void displayResultsScreen() {
        binding.activityMainResultsContainer.setVisibility(View.VISIBLE);
    }

    public void hideResultsScreen() {
        binding.activityMainResultsContainer.setVisibility(View.GONE);
    }

    public void setStartPoint(final LatLng point) {
        infoFragment.setStartPoint();
    }

    public void setStopPoint(final LatLng point) {
        displayLoadingScreen();
    }

    public void clearPoints() {
        hideLoadingScreen();
        cabifymapFragment.clear();
        infoFragment.hideClearButton();
        infoFragment.setStartMessage();
        hideResultsScreen();
        resultsFragment.setData(null);
    }

    public void onInformationReceived(final List<CabifyResponse> responseList) {
        resultsFragment.setData(responseList);
        displayResultsScreen();
        hideLoadingScreen();
    }

    public void onError(final String message) {
        clearPoints();
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

}
