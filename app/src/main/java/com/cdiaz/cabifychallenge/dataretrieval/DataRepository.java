package com.cdiaz.cabifychallenge.dataretrieval;

import android.content.Context;

import com.cdiaz.cabifychallenge.dataretrieval.remote.ApiManager;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.contracts.GenericContract;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by carlos on 20/03/2018.
 */

public class DataRepository {

    private ApiManager apiManager;

    public DataRepository() {
        apiManager = new ApiManager();
    }

    public void getEstimate(final LatLng start, final LatLng stop, final GenericContract callbackContainer) {

        //This is a good place to work on caching :)

        apiManager.getEstimate(start, stop, callbackContainer);

    }



}
