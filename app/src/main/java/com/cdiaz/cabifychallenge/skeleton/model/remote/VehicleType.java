package com.cdiaz.cabifychallenge.skeleton.model.remote;

import com.cdiaz.cabifychallenge.dataretrieval.Icons;
import com.google.gson.annotations.SerializedName;

/**
 * Created by carlos on 20/03/2018.
 */

public class VehicleType {

    @SerializedName("name")
    public String name;

    @SerializedName("icons")
    public Icons icons;

}
