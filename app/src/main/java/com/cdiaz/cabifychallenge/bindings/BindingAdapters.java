package com.cdiaz.cabifychallenge.bindings;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by carlos on 21/03/2018.
 */

public class BindingAdapters {


    @BindingAdapter("imageUrl")
    public static void setImageUrl(final ImageView imageView, final String imageUrl) {

        final RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(new ColorDrawable(Color.WHITE));
        requestOptions.error(new ColorDrawable(Color.GRAY));

        if (imageUrl == null) {
            imageView.setVisibility(View.GONE);
        }
        else {
            imageView.setVisibility(View.VISIBLE);

            Glide.with(imageView.getContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(imageUrl)
                    .into(imageView);
        }
    }
}
