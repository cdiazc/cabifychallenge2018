package com.cdiaz.cabifychallenge.skeleton.views.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cdiaz.cabifychallenge.R;
import com.cdiaz.cabifychallenge.databinding.FragmentPointselectionBinding;
import com.cdiaz.cabifychallenge.skeleton.views.activities.MainActivity;

/**
 * Created by carlos on 20/03/2018.
 */

public class InfoFragment extends Fragment {

    private MainActivity activity;
    private FragmentPointselectionBinding binding;


    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_pointselection, container, false);
        activity = (MainActivity) getActivity();
        binding = DataBindingUtil.bind(rootView);

        binding.setFragment(this);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        activity = null;
        binding = null;

    }


    private void setMessage(final int res) {
        binding.fragmentPointselectionText.setText(getString(res));
    }

    public void setStartMessage() {
        setMessage(R.string.fragment_pointselection_start_point);
    }

    public void setStopMessage() {
        setMessage(R.string.fragment_pointselection_stop_point);
    }

    public void displayClearButton() {
        binding.fragmentPointselectionClear.setVisibility(View.VISIBLE);
    }

    public void hideClearButton() {
        binding.fragmentPointselectionClear.setVisibility(View.GONE);
    }

    public void setStartPoint() {
        displayClearButton();
        setStopMessage();
    }

    public void clear(final View view) {
        hideClearButton();
        setStartMessage();
        activity.clearPoints();
    }

}
