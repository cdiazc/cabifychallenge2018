package com.cdiaz.cabifychallenge.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by carlos on 20/03/2018.
 */

public class Constants {

    public final static String TAG = "CabifyChallenge";

    public final static LatLng MADRID = new LatLng(40.416393, -3.699222);
}
