package com.cdiaz.cabifychallenge.dataretrieval.remote;

import android.util.Log;

import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyRequest;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;
import com.cdiaz.cabifychallenge.skeleton.model.remote.Point;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.contracts.GenericContract;
import com.cdiaz.cabifychallenge.util.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlos on 20/03/2018.
 */

public class ApiManager {

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://test.cabify.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private final ApiController api = retrofit.create(ApiController.class);


    public void getEstimate(final LatLng start, final LatLng stop, final GenericContract callbackContainer) {

        final CabifyRequest request = new CabifyRequest();
        final Point startPoint = new Point();
        startPoint.loc.add(start.latitude);
        startPoint.loc.add(start.longitude);

        final Point stopPoint = new Point();
        stopPoint.loc.add(stop.latitude);
        stopPoint.loc.add(stop.longitude);

        request.stops.add(startPoint);
        request.stops.add(stopPoint);


        final Call<List<CabifyResponse>> call = api.getEstimate(request);

        call.enqueue(new Callback<List<CabifyResponse>>() {
            @Override
            public void onResponse(final Call<List<CabifyResponse>> call, final Response<List<CabifyResponse>> response) {

                if (response.code() < 400) {
                    callbackContainer.onInformationReceived(response.body());
                }
                else {

                    final Converter<ResponseBody, CabifyResponse> converter = retrofit.responseBodyConverter(CabifyResponse.class, new Annotation[0]);
                    try {
                        final CabifyResponse errors = converter.convert(response.errorBody());
                        callbackContainer.onError(errors.message);
                    } catch (final Exception e) {
                        callbackContainer.onError("Error: " + e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(final Call<List<CabifyResponse>> call, final Throwable t) {
                callbackContainer.onError(t.getMessage());
            }
        });


    }



}
