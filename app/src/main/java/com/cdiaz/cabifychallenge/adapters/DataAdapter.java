package com.cdiaz.cabifychallenge.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cdiaz.cabifychallenge.R;
import com.cdiaz.cabifychallenge.databinding.DataElementBinding;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;

import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private List<CabifyResponse> data;

    public DataAdapter(final List<CabifyResponse> data) {
        this.data = data;
    }

    public void setData(final List<CabifyResponse> data) {

        this.data = data;
    }

    @Override
    public DataViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final DataElementBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.data_element, parent, false);
        return new DataViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        final CabifyResponse responseElement = data.get(position);
        holder.bind(responseElement);
    }

    @Override
    public int getItemCount() {
        int ret = 0;

        if (data != null) {
            ret = data.size();
        }

        return ret;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        DataElementBinding binding;

        void bind(final CabifyResponse response) {
            binding.setResponse(response);
            binding.executePendingBindings();
        }

        DataViewHolder(final DataElementBinding bindings) {
            super(bindings.getRoot());
            this.binding = bindings;
        }
    }
}
