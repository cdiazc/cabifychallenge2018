package com.cdiaz.cabifychallenge.skeleton.presenters;

import com.cdiaz.cabifychallenge.dataretrieval.DataRepository;
import com.cdiaz.cabifychallenge.skeleton.presenters.contracts.CabifymapFragmentPresenterContract;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.contracts.CabifymapFragmentContract;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by carlos on 20/03/2018.
 */

public class CabifymapFragmentPresenter implements CabifymapFragmentPresenterContract {

    private CabifymapFragmentContract view;
    private DataRepository repository;

    public CabifymapFragmentPresenter(final CabifymapFragmentContract view,  final DataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void getEstimate(final LatLng start, final LatLng stop) {

        repository.getEstimate(start, stop, view);
    }


}
