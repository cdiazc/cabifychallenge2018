package com.cdiaz.cabifychallenge.util;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by carlos on 20/03/2018.
 */

public class MapUtil {


    public static void drawMarker(final GoogleMap map, final LatLng point) {
        map.addMarker(new MarkerOptions().position(point).draggable(false).visible(true));
    }

    public static void clearMarkers(final GoogleMap map) {
        map.clear();
    }

    public static void centerMap(final GoogleMap googleMap, final LatLng position) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(position);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }
}
