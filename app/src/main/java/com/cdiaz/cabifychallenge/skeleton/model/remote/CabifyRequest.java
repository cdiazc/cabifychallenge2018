package com.cdiaz.cabifychallenge.skeleton.model.remote;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public class CabifyRequest {


    @SerializedName("stops")
    public List<Point> stops = new ArrayList<>();


}
