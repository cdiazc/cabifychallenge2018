package com.cdiaz.cabifychallenge.skeleton.views.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cdiaz.cabifychallenge.R;
import com.cdiaz.cabifychallenge.adapters.DataAdapter;
import com.cdiaz.cabifychallenge.databinding.FragmentResultsBinding;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;
import com.cdiaz.cabifychallenge.skeleton.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public class ResultsFragment extends Fragment {

    private MainActivity activity;

    private RecyclerView recyclerView;
    private DataAdapter adapter;

    private FragmentResultsBinding binding;



    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_results, container, false);
        activity = (MainActivity) getActivity();

        binding = DataBindingUtil.bind(rootView);
        binding.setFragment(this);

        recyclerView = binding.fragmentResultsList;

        adapter = new DataAdapter(new ArrayList<CabifyResponse>());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity.getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        activity = null;
        recyclerView = null;
        adapter = null;
        binding = null;
    }


    public void setData(final List<CabifyResponse> responseList) {
        adapter.setData(responseList);
        adapter.notifyDataSetChanged();
    }

    public void clear(final View view) {
        activity.clearPoints();
    }
}
