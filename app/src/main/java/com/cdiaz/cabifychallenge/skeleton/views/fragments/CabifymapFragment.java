package com.cdiaz.cabifychallenge.skeleton.views.fragments;

import android.os.Bundle;
import android.util.Log;

import com.cdiaz.cabifychallenge.dataretrieval.DataRepository;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;
import com.cdiaz.cabifychallenge.skeleton.presenters.CabifymapFragmentPresenter;
import com.cdiaz.cabifychallenge.skeleton.views.activities.MainActivity;
import com.cdiaz.cabifychallenge.skeleton.views.fragments.contracts.CabifymapFragmentContract;
import com.cdiaz.cabifychallenge.util.Constants;
import com.cdiaz.cabifychallenge.util.MapUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public class CabifymapFragment extends SupportMapFragment implements OnMapReadyCallback, CabifymapFragmentContract {

    private CabifymapFragmentPresenter presenter;
    private DataRepository repository;
    private MainActivity activity;
    private GoogleMap map;

    private LatLng startPoint;
    private LatLng stopPoint;

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        repository = new DataRepository();
        presenter = new CabifymapFragmentPresenter(this, repository);
        activity = (MainActivity) getActivity();

        getMapAsync(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        activity = null;
        presenter = null;
        repository = null;
        map = null;
        startPoint = null;
        stopPoint = null;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        this.map = googleMap;

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng point) {
                onMapClicked(point);
            }
        });

        MapUtil.centerMap(googleMap, Constants.MADRID);

    }

    private void onMapClicked(final LatLng point) {

        if (startPoint == null || stopPoint == null) {
            MapUtil.drawMarker(map, point);
        }

        if (startPoint == null) {
            setStartPoint(point);
        }
        else {
            setStopPoint(point);
        }
    }

    private void setStartPoint(final LatLng point) {
        startPoint = point;
        activity.setStartPoint(point);
    }

    private void setStopPoint(final LatLng point) {
        stopPoint = point;
        activity.setStopPoint(point);
        presenter.getEstimate(startPoint, stopPoint);
    }

    public void clear() {
        startPoint = null;
        stopPoint = null;
        MapUtil.clearMarkers(map);
    }

    @Override
    public void onInformationReceived(final List<CabifyResponse> responseList) {
        activity.hideLoadingScreen();
        activity.onInformationReceived(responseList);
        Log.d(Constants.TAG, "onInformationReceived");
    }

    @Override
    public void onError(final String message) {
        activity.onError(message);
        Log.d(Constants.TAG, "onError " + message);
    }


}
