package com.cdiaz.cabifychallenge.dataretrieval.remote;

import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyRequest;
import com.cdiaz.cabifychallenge.skeleton.model.remote.CabifyResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by carlos on 20/03/2018.
 */

public interface ApiController {

    @Headers({
             "Content-Type: application/json",
             "Authorization: Bearer 6o_FrppOEQ5RrCkBOEKaBM-puJleMKrRn5nW_cy7H9Y"
    })
    @POST("api/v2/estimate")
    Call<List<CabifyResponse>> getEstimate(@Body CabifyRequest request);

}
